# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 09:14:04 2016

@author: jeroen
"""

import numpy as np
import matplotlib .pyplot as plt
#import matplotlib.animation as animation
import robot

r = robot.Robot()

N = 15

a1 = np.linspace(0, np.pi / 6, N)
a2 = np.linspace(0, np.pi / 2, N)
pos = r.forward_kinematics(a1, a2)

#anim = r.animate(a1, a2)

(a1bis, a2bis) = r.inverse_kinematics(pos)

#anim = r.animate(a1bis, a2bis)

# obstacle
def obstacle(x, y):
    """Obstacle: return true if point x, y collides with obstacle"""
    
    # wall at x = 1.3
    return x >= 1.3

j1 = np.linspace(-np.pi / 2, np.pi / 2, N)
j2 = np.linspace(-np.pi, np.pi, N)
j1, j2 = np.meshgrid(j1, j2)
j1 = j1.ravel()
j2 = j2.ravel()

xy = r.forward_kinematics(j1, j2)

index_collision = np.sum(obstacle(xy[0], xy[1]), axis=0)
xy_collision = xy[:, :, index_collision != 0]
xy_free = xy[:, :, index_collision == 0]

plt.figure()
plt.plot(xy_collision[0], xy_collision[1], 'ro-', xy_free[0], xy_free[1], 'go-')
plt.hold('on')
plt.plot([1.3, 1.3], [-3, 3], 'k--')

# plot obstacle in joint space
(j1_c, j2_c) = r.inverse_kinematics(xy_collision)
(j1_f, j2_f) = r.inverse_kinematics(xy_free)

plt.figure()
plt.plot(j1_c, j2_c, 'ro', j1_f, j2_f, 'go')

#g = np.linspace(-3, 3, N)
#xg, yg = np.meshgrid(g, g)
#xg, yg = xg.ravel(), yg.ravel()
#xyg = np.vstack((xg, yg))
#
#xyg_c = xyg[:, obstacle(xyg[0], xyg[1]) == True]
#xyg_f = xyg[:, obstacle(xyg[0], xyg[1]) == False]
#
#plt.figure()
#plt.plot(xyg_c[0], xyg_c[1], 'ro', xyg_f[0], xyg_f[1], 'go')


