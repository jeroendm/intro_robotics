# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 09:11:50 2016
Two link manipulator

@author: jeroen
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

class Robot:
    """Two link manipulator robot"""
    
    def __init__(self, l1=1, l2=1):
        self.l1 = l1
        self.l2 = l2
        
    def __str__(self):
        return "This is a two link manipulator"

    def forward_kinematics(self, a1, a2):
        """Two angles a1, a2 to position of link end points"""
        
        l1 = self.l1
        l2 = self.l2
        
        x1 = l1 * np.cos(a1)
        y1 = l1 * np.sin(a1)
        x2 = l1 * np.cos(a1) + l2 * np.cos(a1 + a2)
        y2 = l1 * np.sin(a1) + l2 * np.sin(a1 + a2)
        
        pos = np.array([[x1, x2], [y1, y2]])
        # always add origin as first point
        pos = np.insert(pos, 0, 0, axis=1)
        
        return pos
        
    def inverse_kinematics(self, pos):   
        """Link end points to angles a1 and a2"""
        x1 = pos[0, 1, :]
        y1 = pos[1, 1, :]
        a1 = np.arctan2(y1, x1)
        
        x2 = pos[0, 2, :]
        y2 = pos[1, 2, :]
        a2 = np.arctan2((y2 - y1), (x2 - x1)) - a1

        return (a1, a2)
        
    def __animate_cartesian(self, x, y):
        """Show animation based on x and y coordinates of link end points"""
        
        nframes = x.shape[1]         
        
        fig1 = plt.figure()
        plt.axis([-0.5, 3, -0.5, 3])
        plt.xlabel('x')
        plt.ylabel('y')
        line, = plt.plot([], [], 'o-')
        
        def init():
            line.set_data([], [])
            return line,
            
        def animate(i):
            xa = x[:, i]
            ya = y[:, i]
            line.set_data(xa, ya)
            return line,
            
        return animation.FuncAnimation(fig1, animate, init_func = init, frames = nframes, interval = 20)
        
    def animate(self, a1, a2):
        pos = self.forward_kinematics(a1, a2)
        pos = np.insert(pos, 0, 0, axis=1) # add origin
        
        return self.__animate_cartesian(pos[0], pos[1])