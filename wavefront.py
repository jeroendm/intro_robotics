# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 11:37:28 2016

@author: jeroen
"""

import numpy as np

# setup a space with an obstacle
N = 10
space = np.zeros((N, N))
space[3:7, 3:7] = 1
print(space)

# move from start to stop
i_start, j_start = 1, 2
i_stop, j_stop = 8, 8

# setup matrix for wavefront
sea = np.zeros(space.shape)
sea[i_stop, j_stop] = 2
sea[i_start, j_start] = -1
sea[space == 1] = 1

print(sea) # check matrix

def neighbours(i, j, nrows, ncols):
    row_index = []
    col_index = []
    if i < (nrows - 1):
        row_index.append(i+1)
        col_index.append(j)
    if i > 0:
        row_index.append(i - 1)
        col_index.append(j)
    if j < (ncols - 1):
        row_index.append(i)
        col_index.append(j + 1)
    if j > 0:
        row_index.append(i)
        col_index.append(j - 1)
    
    return row_index, col_index

def wave(old_sea):
    sea = old_sea.copy()
    nrows = sea.shape[0]
    ncols = sea.shape[1]
    
    for i in range(0, nrows):
        for j in range(0, ncols):
            
            if sea[i, j] > 1:
                steps = sea[i, j] + 1
                x, y = neighbours(i, j, N, N)
                sea[x, y] = steps
    return sea

def propagate_wave(in_sea):
    out_sea = in_sea.copy()
    max_iter = 10
    
    while (out_sea == 0).any() and max_iter > 0:
        out_sea = wave(out_sea)
        print(out_sea, max_iter)
        max_iter -= 1
        
    return out_sea

def get_path(sea):
    i = i_start
    j = j_start
    return np.argmin([sea[i + 1, j], sea[i + 1, j], ])
    

print(propagate_wave(sea))